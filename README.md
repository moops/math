# Math

## Purpose
This repository is for working through math textbooks and developing an
understanding of math and its usefullness.  This is not for racing through a
semester on a deadline, but for actual useful comprehension.  While others may
find my methods helpful, this repository at this point is an attempt to both 
keep and expand on what I‘ve learned already so I can do what I enjoy and share 
it with others.

## Methods
Text books and life are the primary tools used in developing notes and lessons 
here.  Demonstrations and applications use programs and the gitlab markdown 
LaTex engine. I plan to use bash, C, Python, and Scala for working through 
the programs, to varying degrees.  Reasoning follows:

### Bash
I encourage everyone to use their computer effectively.  That implies expanding past images and the mouse and learning about scripting and automation.  Bash is an excellent choice for this as it is available out of the box on OSX and Linux, and is now integrated into Windows 10’s subsystem for linux.  It has a huge supply of tools to use including curl, sed, and awk, and it enables the development and use of small programs, which is perfect for this project. Some se of bc may happen but I don’t foresee the use of sed or awk.  The bash stuff 
will likely focus on git, make, vim or emacs, and scripting various tests.

### C
C is a workhorse, and shares similar syntax with a large number of programming 
languages, allowing the student to pick up other languages easier. It provides 
flexibility for the programmer, is performant, and makes it easier to recognize 
bad code, in my experience. C also is a small language and is easy to pick up.
Every programming demo will have a C implementation. Future potential 
for working with large datasets in some demonstrations will be able to use gpu 
parallelism in cuda or openCl in C.

### Python
Python is a flexible language commonly used in engineering and data science. 
It is easy to set up and integrates well with C and bash.  Some people nonsensically 
call it a scripting language. The scipy and numpy libraries do well in math and 
science applications.  Python applications can use parallelism for large datasets 
with spark. All programming demos will have a python implementation.

### Scala
Scala is a JVM language that blends functional and OO paradigms. It allows use 
of existing Java source, while allowing flexability for the programmer. Scala 
embraces abstraction with vigor. Since spark was written in Scala, large dataset 
problems can use spark in scala as well. All programing demos will have a 
Scala implementation.

### Why not my language?
C is good enough to introduce common syntax stylings, while scala and python 
provide OO and functional paradigms. While javascript is suitable and would provide 
actual object oriented programming examples instead of the class oriented 
approach of the others, javascript does not handle parallelism the same.  I felt 
the combination of C and python covered enough that C++ was not needed, and the 
combination of C and Scala negated the need for Java. 

## Notes and Lessons
Notes and lessons will be in the wiki. It will start with Linear Algebra before 
it branches to Calculus, Physics, and Discrete Math.

## Code
Code will be in this repository, organized by language. a 